package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * Implémentation Base64 de désérialiseur de Badge Digital, basée sur des flux sur base JSON.
 */
public class JSONWalletDeserializerDAImpl implements DirectAccessDatabaseDeserializer, JSONWalletBonusDeserializer {

    private static final Logger LOG = LogManager.getLogger(JSONWalletDeserializerDAImpl.class);

    private OutputStream sourceOutputStream;
    Set<DigitalBadge> metas;

    /**
     * Constructeur élémentaire
     *
     * @param sourceOutputStream
     * @param metas              les métadonnées du wallet, si besoin
     */
    public JSONWalletDeserializerDAImpl(OutputStream sourceOutputStream, Set<DigitalBadge> metas) {
        this.setSourceOutputStream(sourceOutputStream);
        this.metas = metas;
    }

    /**
     * constructeur par défaut
     */
    public JSONWalletDeserializerDAImpl() {

    }

    /**
     * {@inheritDoc}
     *Avec amélioration pour détecter la fraude
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException {
        // targetBadge = new DigitalBadge();
        long pos = targetBadge.getMetadata().getWalletPosition();
        media.getChannel().seek(pos);

        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

        String data = media.getEncodedImageReader(false).readLine();//changé à true
        String badgeData = data.split(",\\{\"payload")[0].split(".*badge\":")[1];
        int badgeId = Integer.parseInt(data.split("badgeId\":")[1].split(",")[0]);
        System.out.println("badgeId: " + badgeId + " " + targetBadge.getMetadata().getBadgeId());
        long walletPosition = Long.parseLong(data.split("walletPosition\":")[1].split(",")[0]);
        System.out.println("walletPosition: " + walletPosition + " " + targetBadge.getMetadata().getWalletPosition());
        long imageSize = Long.parseLong(data.split("imageSize\":")[1].split("\\}")[0]);
        System.out.println("imageSize: " + imageSize + " " + targetBadge.getMetadata().getImageSize());
        String serial = data.split("serial\":\"")[1].split("\"")[0];
        Date begin = new Date(Long.parseLong(data.split("begin\":")[1].split(",")[0]));
        System.out.println("begin: " + begin + " " + new Date(targetBadge.getBegin().getTime()));
        Date end = new Date(Long.parseLong(data.split("end\":")[1].split("\\}")[0]));
        System.out.println("end: " + end + " " + new Date(targetBadge.getEnd().getTime()));
        DigitalBadge badge = objectMapper.readValue(badgeData, DigitalBadge.class);
        String encodedImageData = "";
        //   System.out.println("targetBadge: " + targetBadge.toString());
        System.out.println("targetBadge: " + targetBadge.toString());
        if (targetBadge != null) { //pour détecter la fraude
            if (badgeId != targetBadge.getMetadata().getBadgeId() || walletPosition != targetBadge.getMetadata().getWalletPosition() || imageSize != targetBadge.getMetadata().getImageSize() || !targetBadge.getSerial().equals(serial) || begin.compareTo(targetBadge.getBegin()) != 0 || end.compareTo(targetBadge.getEnd()) != 0) {
                encodedImageData = data.split(",\\{\"payload\":\"")[1].split("\"\\}]")[0];
                try (OutputStream os = getSourceOutputStream()) {
                    getDeserializingStream(encodedImageData).transferTo(os);
                }
                targetBadge.setSerial(badge.getSerial());
                targetBadge.setBegin(badge.getBegin());
                targetBadge.setEnd(badge.getEnd());
                throw new IOException("Possible tentative de fraude");
            }
        }

        encodedImageData = data.split(",\\{\"payload\":\"")[1].split("\"\\}]")[0];//correction
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(encodedImageData).transferTo(os);
        }
        targetBadge.setSerial(badge.getSerial());
        targetBadge.setBegin(badge.getBegin());
        targetBadge.setEnd(badge.getEnd());
    }

    /**
     * {@inheritDoc}
     *
     * @return OutputStream
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     *
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
        this.sourceOutputStream = os;
    }


    /**
     * {@inheritDoc}
     * <p>
     * Inutile => Non pris encharge par cet Objet
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {
        // Inutile
        throw new IOException("Non pris encharge par cet Objet");
    }

    /**
     * {@inheritDoc}
     *
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }


    /**
     * {@inheritDoc}
     * Désérialise par badgePosition
     *
     * @param media
     * @param pos
     * @throws IOException
     */
    @Override
    public DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException {
        String data;
        DigitalBadge badge = new DigitalBadge();
        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);
        while ((data = media.getEncodedImageReader(true).readLine()) != null) {//resume true!!!
            long walletPosition = Long.parseLong(data.split("walletPosition\":")[1].split(",")[0]);
            long imageSize = Long.parseLong(data.split("imageSize\":")[1].split("\\}")[0]);
            if (imageSize == -1) break;
            if (walletPosition == pos) {
                String badgeData = data.split("\\}\\]")[0].split(".*badge\":")[1];
                badge = objectMapper.readValue(badgeData, DigitalBadge.class);
                String encodedImageData = data.split(",\\{\"payload\":\"")[1].split("\"\\}]")[0];//corrigé
                // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
                JSONWalletDeserializerDAImpl deserializer = new JSONWalletDeserializerDAImpl();//instantiation pour désérialiser en fct de media et badge...
                try {
                    deserializer.deserialize(media, badge);//...Ici
                } catch (NullPointerException e) {
                    return badge;
                }
                return badge;
            }
        }
        return null;
    }

    /**
     * Desérialise par metadatas
     * {@inheritDoc}
     *
     * @param media
     * @param metaDatas
     * @return
     * @throws IOException
     */
    @Override
    public DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metaDatas) throws IOException {
        String data;
        DigitalBadge badge;
        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);
        while ((data = media.getEncodedImageReader(true).readLine()) != null) {//resume true!!!
            int badgeId = Integer.parseInt(data.split("badgeId\":")[1].split(",")[0]);
            long walletPosition = Long.parseLong(data.split("walletPosition\":")[1].split(",")[0]);
            long imageSize = Long.parseLong(data.split("imageSize\":")[1].split("\\}")[0]);
            if (imageSize == -1) break;
            if (walletPosition == metaDatas.getWalletPosition() && badgeId == metaDatas.getBadgeId() && imageSize == metaDatas.getImageSize()) {
                String badgeData = data.split("\\}\\]")[0].split(".*badge\":")[1];
                badge = objectMapper.readValue(badgeData, DigitalBadge.class);
                String encodedImageData = data.split(",\\{\"payload\":\"")[1].split("\"\\}]")[0];//corrigé
                // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
                JSONWalletDeserializerDAImpl deserializer = new JSONWalletDeserializerDAImpl();//instantiation pour désérialiser en fct de media et badge...
                try {
                    deserializer.deserialize(media, badge);//...Ici
                } catch (NullPointerException e) {
                    return badge;
                }
                return badge;
            }
        }
        return null;
    }
}