package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;

public interface JSONWalletBonusDeserializer {
    /**
     * Desérialise d'un media JSON et de metadatas
     * @param media
     * @param metadatas
     * @throws IOException
     */
    DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metadatas) throws IOException;

    /**
     * Désérialise d'un media JSON et d'une position dans le média
     * @param media
     * @param pos
     * @throws IOException
     */
    DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException;
}
