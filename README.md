# Applications WEB, MVC, Services Rest hello
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Génération et Parsage JSON 

Notre startup a fait le buzz auprès de ses investisseurs grâce au prototype développé avec Swing. 
Le hic, c'est qu'à présent, pour avancer, nous devons accepter l'introduction d'un nouvel investisseur prenant le rôle de CTO, car ayant la confiance du BOard en matière d'industrialisation logicielle.
Ce nouveau CTO ainsi proclamé, un certain nombre de pré-requis nous sont alors imposés.

# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Génération et Parsage JSON 

Notre startup a fait le buzz auprès de ses investisseurs grâce au prototype développé avec Swing. 
Le hic, c'est qu'à présent, pour avancer, nous devons accepter l'introduction d'un nouvel investisseur prenant le rôle de CTO, car ayant la confiance du BOard en matière d'industrialisation logicielle.
Ce nouveau CTO ainsi proclamé, un certain nombre de pré-requis nous sont alors imposés.

 1. prérequis n°1: passer par une architecture centralisée Web 2.0. Incontournablement, afin de pouvoir progresser sur un business model rentable, évoluer ver le Web semble la meilleure option. On touche ainsi une plus large clientèle, avec des possibilités de monétisation bien plus efficaces.
 2. prérequis n°2: utiliser le format RFC8259 (JSON) de façon stricte et généralisée pour toute sérialisation ou stockage d'information. Peut-être notre CTO aurait-il des ambitions à plus long terme avec un passage sur le cloud potentiellement plus efficace ?
 3. ...

## Objectifs
* Mises en application:
- [x] (Exercice 1) Normalisation JSON en pré-requis au passage en application WEB
- [x] **(Exercice 2) BONUS de méthodes utilitaires, exceptions pour détection de fraude, Utilisation d'un Tableau associatif**
- [ ] (Exercice 3) Homogénéisation Maven ET Spring avancé
- [ ] (Exercice 4) Services Rest et Scaffolding web basique

----

## Surcharge de méthodes

Le cas est trivial, et comme l'indique avec précision le diagramme ci-après, voici les nouvelles méthodes à implémenter:

 - [ ] Dans **JSONBadgeWalletDAOImpl** il s'agit de permettre notamment de récupérer les méta-données dans un obet de type Map 
     - [ ] **public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException** 
     - [ ] **public SortedMap<DigitalBadge, DigitalBadgeMetadata> getWalletMetadataMap() throws IOException**
 - [ ] Pour **JSONWalletDeserializerDAImpl**, se servir de cette interface pour implémenter les 2 méthodes:
   ```java
    public interface JSONWalletBonusDeserializer extends DirectAccessDatabaseDeserializer {
        DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException ;
        DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException;
    }
    ```
     - [ ] **public DigitalBadge deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas) throws IOException**
     - [ ] **public DigitalBadge deserialize(WalletFrameMedia media, long pos) throws IOException**
      - [ ] Il s'agit d'une rationnalisation du code de la méthode **public void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException**
      - [ ] En fait, veiller à ce que toutes les méthodes de désérialisation de cette classe ne soit que des surcharge de **deserialize(WalletFrameMedia media, long pos)**, cela ne fera qu'alléger et améliorer le code.

```plantuml

@startuml

title __JSON Serialization BONUS's Class Diagram__\n

  namespace fr.cnam.foad.nfa035.badges.wallet.dao {
    namespace impl.json {
        interface fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO {
            {abstract} + getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta)
            {abstract} + getWalletMetadataMap()
        }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.dao {
    namespace impl.json {
        class fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl {
            {static} - LOG : Logger
            - walletDatabase : File
            + JSONBadgeWalletDAOImpl(...)
            + addBadge(...)
            + addBadge(...)
            + getBadge(...)
            + getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta)
            + getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta)
            + getWalletMetadata()
            + getWalletMetadataMap()
            - getWalletMetadata(String mode)
        }
    }
  }


  fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl .up.|> fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO

  namespace fr.cnam.foad.nfa035.badges.wallet.dao {
    interface fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO {
        {abstract} + addBadge(...)
        {abstract} + getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta)
        {abstract} + getWalletMetadata()
    }
  }

  fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO .up.|> fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO


  namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    namespace impl.db.json {
          interface fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletBonusDeserializer {
             {abstract}  + deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas)
             {abstract}  + deserialize(WalletFrameMedia media, long pos)
          }
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer {
    namespace impl.db.json {
      class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl {
          ~ metas : Set<DigitalBadge>
          {static} - LOG : Logger
          - sourceOutputStream : OutputStream
          + JSONWalletDeserializerDAImpl(...)
          + deserialize(WalletFrameMedia media, DigitalBadge targetBadge)
          + deserialize(WalletFrameMedia media, DigitalBadgeMetadata metas)
          + deserialize(WalletFrameMedia media, long pos)
          + getDeserializingStream()
          + getSourceOutputStream()
          + setSourceOutputStream(...)
          - refreshBadgeMetadata(DigitalBadge originBadge, DigitalBadge targetBadge)
      }
    }
  }


  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl .up..|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletBonusDeserializer
  
@enduml
```

## Détection de Fraude ?

 - [ ] Détection de fraude ou pas, en tout cas on veut que dans e cas où l'on tente de récupérer un Badge à partir de Métadonnées incohérente, cela déclenche une exception.
  - [ ] Plus simplement, veillez à ce que ce Test compile et passe au vert ^^
   ```java
   @Test
    public void testGetBadgeFromDatabaseByMetadataWithFraud(){
        try {
            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet.json");

            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");
            DigitalBadge expectedBadge1 = new DigitalBadge("SUPER_STAR", begin, end, new DigitalBadgeMetadata(1, 0,557), null);

            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            assertThrows(IOException.class, () -> {
                dao.getBadgeFromMetadata(fileBadgeStream1, expectedBadge1);
            });

            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");

        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }
   ```
 - [ ] Enfin merci pour vostre travail soutenu et les preuves que vous me donnez...
   ![preuve][preuve]

[preuve]: screenshots/All-Tests-4.png "20 tests"


----
